{ # Use master branch of the overlay by default
  mozillaOverlay ? import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz),
}:

let
  pkgs = import <nixpkgs> { overlays = [ mozillaOverlay ]; };
  targets = [
    "thumbv7em-none-eabihf"
  ];
  rustManifest = pkgs.fetchurl {
    url = "https://static.rust-lang.org/dist/2024-06-13/channel-rust-stable.toml";
    sha256 = "sha256-Ngiz76YP4HTY75GGdH2P+APE/DEIx2R/Dn+BwwOyzZU=";
  };
  rustChannelOfTargets = _channel: _date: targets:
    (pkgs.lib.rustLib.fromManifestFile rustManifest {
      inherit (pkgs) stdenv lib fetchurl patchelf;
      }).rust.override {
      inherit targets;
      extensions = ["rust-src"];
    };
  rust = rustChannelOfTargets "nightly" null targets;
  rustPlatform = pkgs.recurseIntoAttrs (pkgs.makeRustPlatform {
    rustc = rust;
    cargo = rust;
  });

  buildStm32Firmware = { name, src, cargoDepsName ? name, cargoPatches ? [], patchPhase ? "", extraNativeBuildInputs ? [], checkPhase ? "", doCheck ? true, binaryName ? name, extraCargoBuildArgs ? "", outputHashes ? {} }:
    rustPlatform.buildRustPackage rec {
      inherit name cargoDepsName;
      version = "0.0.0";

      inherit src cargoPatches;

      cargoLock = { lockFile = "${src}/Cargo.lock"; inherit outputHashes; };

      inherit patchPhase;
      nativeBuildInputs = [ pkgs.llvm ] ++ extraNativeBuildInputs;
      buildPhase = ''
        export CARGO_HOME=$(mktemp -d cargo-home.XXX)
        cargo build --release --bin ${binaryName} ${extraCargoBuildArgs}
      '';

      inherit checkPhase doCheck;
      # binaryName defaults to the `name` arg (i.e. the Rust package name);
      # it is used as the Cargo binary filename
      installPhase = ''
        mkdir -p $out $out/nix-support
        cp target/thumbv7em-none-eabihf/release/${binaryName} $out/${name}.elf
        echo file binary-dist $out/${name}.elf >> $out/nix-support/hydra-build-products
        llvm-objcopy -O binary target/thumbv7em-none-eabihf/release/${binaryName} $out/${name}.bin
        echo file binary-dist $out/${name}.bin >> $out/nix-support/hydra-build-products
      '';

      dontFixup = true;
      auditable = false;
    };
in
  pkgs.lib.attrsets.mapAttrs'
    (name: value: pkgs.lib.attrsets.nameValuePair ("stabilizer-" + name)
    (buildStm32Firmware ({
      name = "stabilizer-" + name;
      # If binaryName is not specified, use the attribute name as binaryName by default.
      binaryName = name;
      cargoDepsName = "stabilizer";
      src = <stabilizerSrc>;
      patchPhase = ''
        patch -p1 < ${./pounder-725.diff}
      '';
      cargoPatches = [
        ./pounder-cargo-patch.diff
      ];
      doCheck = false;
    } // value))) {
      dual-iir = {};
      dual-iir-pounder_v1_0 = {
        binaryName = "dual-iir";
        extraCargoBuildArgs = "--features pounder_v1_0";
      };
      lockin = {};
    } //
  {
    # openMMC build system breaks if host compiler is not available, so do not use stdenvNoCC here
    sayma-mmc = pkgs.stdenv.mkDerivation {
      name = "sayma-mmc";
      src = <saymaMmcSrc>;
      phases = [ "unpackPhase" "buildPhase" "installPhase" ];
      nativeBuildInputs = [ pkgs.cmake pkgs.gcc-arm-embedded ];
      buildPhase =
        ''
        mkdir build
        cd build
        cmake .. -DBOARD=sayma -DBOARD_RTM=sayma -DVERSION= -DTARGET_CONTROLLER=LPC1776 -DCMAKE_BUILD_TYPE=Debug
        make
        '';
      installPhase =
        ''
        mkdir $out
        cp out/* $out
        mkdir -p $out $out/nix-support
        echo file binary-dist $out/openMMC.axf >> $out/nix-support/hydra-build-products
        echo file binary-dist $out/openMMC.bin >> $out/nix-support/hydra-build-products
        '';
    };
  }

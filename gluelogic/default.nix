{ pkgs ? import <nixpkgs> {} }:
let
  ise = import ./ise.nix { inherit pkgs; };
  vivado = import ./vivado.nix { inherit pkgs; };
  fpgatools = import ./fpgatools.nix { inherit pkgs; };
  buildUrukulCpld = {version, src}: pkgs.stdenv.mkDerivation {
    pname = "urukul-cpld";
    inherit src version;
    buildInputs = [(pkgs.python3.withPackages(ps: [fpgatools.migen]))] ++ (builtins.attrValues fpgatools.ise);
    phases = ["buildPhase" "installPhase"];
    buildPhase = "python $src/urukul_impl.py";
    installPhase =
      ''
      mkdir -p $out $out/nix-support
      cp build/urukul.jed $out
      echo file binary-dist $out/urukul.jed >> $out/nix-support/hydra-build-products
      '';
  };
  buildMirnyCpld = {version, patchPhase ? "", src}: pkgs.stdenv.mkDerivation {
    pname = "mirny-cpld";
    inherit src version patchPhase;
    buildInputs = [(pkgs.python3.withPackages(ps: [fpgatools.migen]))] ++ (builtins.attrValues fpgatools.ise);
    phases = ["unpackPhase" "patchPhase" "buildPhase" "installPhase"];
    buildPhase = "python $src/mirny_impl.py";
    installPhase =
      ''
      mkdir -p $out $out/nix-support
      cp build/mirny.jed $out
      echo file binary-dist $out/mirny.jed >> $out/nix-support/hydra-build-products
      '';
  };
in
  {
    urukul-cpld-master = buildUrukulCpld {
      version = "master";
      src = <urukulSrc>;
    };
    urukul-cpld-release = buildUrukulCpld rec {
      version = "1.4.0";
      src = pkgs.fetchFromGitHub {
        owner = "quartiq";
        repo = "urukul";
        rev = "v${version}";
        sha256 = "1962jpzqzn22cwkcmfnvwqlj5i89pljhgfk64n6pk73clir9mp0w";
      };
    };
    urukul-cpld-legacy = buildUrukulCpld rec {
      version = "1.3.1";
      src = pkgs.fetchFromGitHub {
        owner = "quartiq";
        repo = "urukul";
        rev = "v${version}";
        sha256 = "1nvarspqbf9f7b27j34jkkh4mj6rwrlmccmfpz5nnzk3h2j6zbqc";
      };
    };
    mirny-cpld-master = buildMirnyCpld {
      version = "master";
      src = <mirnySrc>;
    };
    mirny-cpld-release = buildMirnyCpld rec {
      version = "0.3";
      src = pkgs.fetchFromGitHub {
        owner = "quartiq";
        repo = "mirny";
        rev = "v${version}";
        sha256 = "sha256-u1iXcbGX6JkVgfpnCbkyTOVoMYnYcSufLBb6OBAeu8c=";
      };
    };
    mirny-cpld-legacy-almazny = buildMirnyCpld rec {
      version = "0.2.4";
      src = pkgs.fetchFromGitHub {
        owner = "quartiq";
        repo = "mirny";
        rev = "v${version}";
        sha256 = "sha256-/O1AE0JOXALC8I7NPhOd8h18oX8Qu7lj+6ToAMMD3zs=";
      };
      patchPhase = "patch -p1 < ${./mirny-legacy-almazny.diff}";
    };
    mirny-cpld-almazny = buildMirnyCpld rec {
      version = "0.3";
      src = pkgs.fetchFromGitHub {
        owner = "quartiq";
        repo = "mirny";
        rev = "v${version}";
        sha256 = "sha256-u1iXcbGX6JkVgfpnCbkyTOVoMYnYcSufLBb6OBAeu8c=";
      };
      patchPhase = "patch -p1 < ${./mirny-almazny.diff}";
    };
    fastino-fpga = pkgs.stdenv.mkDerivation {
      name = "fastino-fpga";
      src = <fastinoSrc>;
      buildInputs = [(pkgs.python3.withPackages(ps: [fpgatools.migen fpgatools.misoc]))] ++ [pkgs.yosys pkgs.nextpnr pkgs.icestorm];
      phases = ["buildPhase" "installPhase"];
      buildPhase = "python $src/fastino_phy.py";
      installPhase =
        ''
        mkdir -p $out $out/nix-support
        cp build/fastino.bin $out
        echo file binary-dist $out/fastino.bin >> $out/nix-support/hydra-build-products
        '';
    };
    phaser-fpga = pkgs.stdenv.mkDerivation {
      name = "phaser-fpga";
      src = <phaserSrc>;
      patchPhase = ''
        substituteInPlace phaser.py \
          --replace "source ../load.tcl" \
                    ""
      '';

      buildInputs = [ (pkgs.python3.withPackages(ps: [ fpgatools.migen fpgatools.misoc ])) ] ++ [ fpgatools.vivado ];
      buildPhase = "python phaser.py";
      installPhase =
        ''
        mkdir -p $out $out/nix-support
        cp build/phaser.bit $out
        echo file binary-dist $out/phaser.bit >> $out/nix-support/hydra-build-products
        '';
      dontFixup = true;

      doCheck = true;
      checkInputs = [ pkgs.python3Packages.pytest ];
      checkPhase = "pytest";
    };
  }

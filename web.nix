let
   pkgs = import <nixpkgs> {};
   web-src = <webSrc>;
   nmigen-src = <nmigenSrc>;
in
  rec {
    web = pkgs.runCommand "web" {} "cd ${web-src}; ${pkgs.zola}/bin/zola build -o $out";
    sphinxcontrib-platformpicker = pkgs.python3Packages.buildPythonPackage rec {
      pname = "sphinxcontrib-platformpicker";
      version = "1.3";
      src = pkgs.fetchFromGitHub {
        owner = "whitequark";
        repo = "sphinxcontrib-platformpicker";
        rev = "v${version}";
        sha256 = "sha256-qKhi4QqYhU7CbNSpziFacXc/sWWPSuM3Nc/oWPmBivM=";
       };
      propagatedBuildInputs = [ pkgs.python3Packages.sphinx ];
    };
    nmigen-docs = pkgs.stdenvNoCC.mkDerivation {
      name = "nmigen-docs";
      src = nmigen-src;
      buildInputs = [ (pkgs.python3.withPackages(ps: [ ps.sphinx ps.sphinx_rtd_theme sphinxcontrib-platformpicker ])) ];
      phases = [ "buildPhase" ];
      buildPhase =
        ''
        export PYTHONPATH=$src
        sphinx-build -b html $src/docs $out
        '';
    };
  }
